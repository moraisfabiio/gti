<?php

require_once '../regra/connect.php';

if($_GET['id']){
    $id = $_GET['id'];

    $sql = "select * from cliente where id={$id}";
    $result = $conn->query($sql);
    
    $dados = $result->fetch_assoc();

    $conn->close();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alterar Cliente</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    .container{
        max-width : 500px;
        text-align : center;
        margin-top: 30px;
    }
    h3{
        text-align:center;
    }
</style>
<body>
    <h3>ALTERAR CLIENTE</h3>
    <div class="container">
        <form action="../regra/update.php" method="post">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" value="<?php echo $dados['nome'] ?>">            
            <label for="nome">Data Cadastro</label>
            <input class="form-control" type="text" name="data_cadastro" value="<?php echo $dados['data_cadastro'] ?>">
            <label for="nome">Data Visita</label>
            <input class="form-control" type="text" name="data_visita" value="<?php echo $dados['data_visita'] ?>">
            <br>
            <div class="but">
                <button class="btn btn-success" type="submit">Salvar</button>
                <a href="index.php"><button class="btn btn-danger" type="button">Cancelar</button></a>
            </div>
            <input class="form-control" type="hidden" name="id" value="<?php echo $dados['id'] ?>">
        </form>
    </div>
</body>
</html>