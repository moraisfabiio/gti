<?php
require_once '../regra/connect.php';
    if($_GET['id']){
        $id = $_GET['id'];

        $sql = "select * from cliente where id={$id}";
        $result = $conn->query($sql);
    
        $dados = $result->fetch_assoc();
        $conn->query($sql);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ecluir Cliente</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    .container{
        text-align:center;
    }
</style>
<body>
    
    <form action="../regra/delete.php" method="post">
        <div class="container">
            <h3>Tem certeza que deseja excluir</h3>
            <br>
            <button class="btn btn-success" type='submit'>Confirmar</button>
            <a href='index.php'><button class="btn btn-danger" type='button'>Cancelar</button></a>
            <input type="hidden" name="id" value="<?php echo $dados['id'] ?>">
        </div>
    </form>
    
</body>
</html>
