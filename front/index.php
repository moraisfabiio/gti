<?php 
    require_once '../regra/connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Index</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    .container{
        max-width: 800px;
        margin-top: 15px;
        text-align: center;
    }
    thead{
        background-color: #919090;
    }
</style>
<body>
    <div class="container">
        <div class="but">
            <a href="criar.php"><button class="btn btn-primary" type="button">Adicionar</button></a>
        </div>
        <br>
        <table class="table table-bordered">
            <thead>
                <th>Id</th>
                <th>Nome</th>
                <th>Data Cadastro</th>
                <th>Data Visita</th>
                <th colspan="2"></th>
            </thead>
            <tbody>
                <?php 
                    $sql = "select * from cliente";
                    $result = $conn->query($sql);
                    if($result->num_rows > 0){
                        while($row = $result->fetch_assoc()){
                            echo "<tr>
                                      <td>".$row['id']."</td> 
                                      <td>".$row['nome']."</td>
                                      <td>".$row['data_cadastro']."</td>
                                      <td>".$row['data_visita']."</td>
                                      <td><a href='alterar.php?id=".$row['id']."'><button class='btn btn-primary' type='button'>Editar</button></a></td>
                                      <td><a href='excluir.php?id=".$row['id']."'><button class='btn btn-danger' type='button'>Excluir</button></a></td>
                                  </tr>";
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>    
</body>
</html>