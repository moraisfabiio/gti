<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Criar Cliente</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<style>
    .container{
        max-width: 600px;
        text-align: center;
        margin-top:50px;
    }
   h3{
       text-align: center;
   }
</style>
<body>
    <h3>CADASTRO DE CLIENTE</h3>
    <div class="container">
        <form action="../regra/create.php" method="post">
            <label for="nome">Nome</label>
            <input class="form-control" type="text" name="nome" placeholder="nome">
            <label for="data_cadastro">Data Cadastro</label>
            <input class="form-control" type="date" name="data_cadastro" placeholder="Data de Cadastro">
            <label for="data_visita">Data Visita</label>
            <input class="form-control" type="date" name="data_visita" placeholder="Data de Visita">
            <br>
            <button class="btn btn-success" type="submit">Salvar</button>
            <a href="index.php"><button class="btn btn-danger" type="button">Cancelar</button></a>
        </form>
    </div>
</body>
</html>